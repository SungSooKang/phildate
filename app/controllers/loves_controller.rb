class LovesController < ApplicationController
  before_action :authenticate_user!
  # before_action :set_love, only: [:read]

  def unchecked
    @loves = current_user.loves.unchecked
  end

  def index
    @loves = current_user.loved_loves
    # render text: @s.pluck(:adorable_id,:adorable_type, :user_id)


  end

  def read
    @loves = current_user.loved_loves.last
                 # .where("created_at >= ?",20.seconds.ago)
    # if(@loves.count != 0)
    render :json => @loves.to_json(:include => :user)
    # else
    #   render :json => @loves.to_json(:include => :user)
    # end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # def set_love
  #   @love = Love.find(params[:id])
  # end



end
