class Post < ActiveRecord::Base

	belongs_to :user
	has_many :loves, as: :adorable
	has_many :comments, dependent: :destroy
	acts_as_votable


	def init
		self.like_number ||=0.0
	end
end
