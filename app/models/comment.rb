class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  acts_as_votable
  has_many :loves, as: :adorable

  validates :body, presence: true
end
